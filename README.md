# PolyVent

PolyVent is a completely open, highly buildable, demonstrated mechanical ventilator.
Although it is intended to be the basis of designs that could be used
for human patients, at present is intended for research purposes,
as an educational platform, and possibly for work with animal models.

![PolyVent 2 Educational Platform](./PolyVentPhoto.jpeg)

We have written a [draft User Manual](https://docs.google.com/document/d/1je_TZ1ShpLbFjLXZAFcuHl6_X6VwyOcD0hfw65ebrbM/edit?usp=sharing).


# Status

The PolyVent 2 Educational Platform is being used in classrom exercises
on November 18th, 2022, at Rice University.

Right now, there is one PolyVent prototype.

The PolyVent 2 does not yet have an alarm system (we have prototyped
a [general purpose alarme device (GPAD)](https://github.com/PubInv/general-alarm-device)). It currently does not have a Graphical User Interface but accepts
commands on the serial port enterred via the Arduino IDE, for example.

# Why It Matters

![PolyVent Users](./PolyVentBroshure.png)

## Researchers

Researchers need a ventilation platform which is open and transparent and
allows them to easily modify both the software and the hardware.
PolyVent uses all reciprocal open source hardare and software licenses.
It is extremely modular, maximizing the easy with which a new feature
could be added or tested. This includes ventilation modes, but could
also include hardware features such as acoustic ventilator, percussive
ventilator, humidifiers, etc.

## Educators

The PolyVent II features a transparent "cake dome" design for easy classroom
instruction. Because it is fully modifiable and open, it can be used
as a classroom or enrcichment module in bioengineering, pulmonology,
troubleshooting, embedded programing, electrical engineering,
mechanical engineering.

## Entrepreneurs

Any firm in the world has the right to take the PolyVent design
and design a manufacturuing plan and seek regulatory approval for it.
It is our hope that firms will make money from our basic design.
By doing some of the research and development and testing, we have
lowered the capital expense of developing a marketable ventilator.

# Organization of GitLab Repos and Modules

To maximize the PolyVent modularity, we have placed each
module in its own repo. The list of repos can be found [here](https://gitlab.com/polyvent).

This approach maximizes modifiability, but may make the big picture
harder to see. Please contact us if you have questions, either
by creating an issue here, by emailing the project leaders,
Victor Suturin, PhD [victor.suturin@pubinv.org](mailto:victor.suturin@pubinv.org)
or Robert L. Read, PhD [read.robert@pubinv.org](mailto:read.robert@pubinv.org].

# About Public Invention

PolyVent is currently a project of [Public Invention](https://www.pubinv.org/), a US 501c3 public charity.

# Contact Us If You Want Us to Build You a PolyVent

Although you have a legal right to build a ventilator based on
the PolyVent design so long as you obey all of our free-libre open source
licenses without contacting us, you may wish to confer with us
or to pay us to build one for you.

The parts in a PolyVent cost approximately $1000USD, and we will build
you one for $5,000USD. Contact
Victor Suturin, PhD [victor.suturin@pubinv.org](mailto:victor.suturin@pubinv.org)
or Robert L. Read, PhD [read.robert@pubinv.org](mailto:read.robert@pubinv.org]
if you wish to discuss this.

# We Need Volunteers

This is a (mostly) all-volunteer effort. The PolyVent 2 hardware
is stable, but we need to develop peripherals, classroom educational
materials, and improve the software. If you would like to
improve your skills by working on a project impacting global health
that aims at academic publication, please contact us and we will
setup an video interview.

# DOI

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10642611.svg)](https://doi.org/10.5281/zenodo.10642611)

# OSHWA Certification

![Alt text](image-1.png)

